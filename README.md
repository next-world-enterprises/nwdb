# Nextworld XR Device Bridge CLI

## Commands:

### devices
get a list of attached devices

> `nwdb devices`


### installed
checks if a package is installed

> `nwdb installed [package name]`
  - **[package name]**: the package identifier to install


### lsapps
get a list of all installed apps

> `nwdb lsapps [filter]`
  - **[filter]**: a filter to search by


### lscat
get a list of catalogued apps

> `nwdb lscat`


### verify
returns an error if a package is NOT installed

> `nwdb verify [package name]`
  - **[package name]**: the package identifier to verify


### version
gets the version of adb

> `nwdb version`


### lspd
lists the nextworld packs downloaded to the device

> `nwdb lspd`


### lspi
lists the nextworld packs installed on the device

> `nwdb lspi`


### uninstall
uninstalls an apk

> `nwdb uninstall [package name]`
  - **[package name]**: the package identifier to uninstall


### install
installs an apk

> `nwdb install [apk path]`
  - **[apk path]**: the local path to the apk to be installed


### manifest
reads a manifest

> `nwdb manifest [apk path]`
  - **[apk path]**: the local path to the apk to read to inspect


### print
reads a text file

> `nwdb print [device path]`
  - **[device path]**: the device path for the text file to print


### nwid
gets the nextworld id

> `nwdb nwid`


### os-upgrade
upgrades the attached device to the latest version stored on nextworld infrastructure

> `nwdb os-upgrade`


### os-nwenv
sets the nextworld environment settings on the device

> `nwdb os-nwenv`


### os-kiosk
enable or disable kiosk mode for nextworld agent on the device

> `nwdb os-kiosk [true/false]`
  - **[true/false]**: true for enable, false for disable


### start
starts a catalogued app

> `nwdb start [package name]`
  - **[package name]**: the package identifier for the app to start


### shell
forwards an adb shell command

> `nwdb shell [...args]`
  - **[...args]**: the arguments to forward to shell


### adb
forwards an adb command

> `nwdb adb [...args]`
  - **[...args]**: the arguments to forward to adb


### docs
generates the README.md for this CLI

> `nwdb docs`


