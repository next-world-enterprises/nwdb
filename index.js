module.exports = require("./lib");
if (typeof require !== 'undefined' && require.main === module) {
    require("./lib/cli").run();
}