import { Client, ClientDeviceInfo } from "./Adbkit";
import { AdbDevice } from "./AdbDevice";
export default class AdbClient {
    static FIRST_DEVICE: string;
    client: Client | null;
    onDevicesChanged: (devices: AdbDevice[]) => void;
    onDeviceConnected: (device: AdbDevice) => void;
    onDeviceDisconnected: (device: AdbDevice) => void;
    devices: AdbDevice[];
    constructor();
    init(): Promise<void>;
    disconnect(): Promise<void>;
    trackDevices(): Promise<import("./Adbkit").ClientTracker>;
    getDevice(deviceId: string): AdbDevice;
    requireDevice(deviceId: string): AdbDevice;
    loadDevice(deviceId: string): Promise<AdbDevice>;
    getDevices(): Promise<AdbDevice[]>;
    getVersion(): Promise<string>;
    syncDevices(): Promise<void>;
    wrapDevice(deviceData: ClientDeviceInfo): Promise<AdbDevice>;
}
