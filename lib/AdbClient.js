"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Adbkit_1 = __importDefault(require("./Adbkit"));
const FS = __importStar(require("fs"));
const utils_1 = require("./utils");
const AdbDevice_1 = require("./AdbDevice");
const log_1 = require("./log");
const Paths = __importStar(require("./paths"));
class AdbClient {
    constructor() {
        this.client = null;
        this.devices = [];
        this.init();
    }
    async init() {
        this.client = Adbkit_1.default.createClient({ bin: (0, utils_1.getBinPath)() });
        this.trackDevices();
        process.on("exit", () => {
            this.client.kill();
        });
        if (FS.existsSync(Paths.LOCAL_DATA) === false) {
            FS.mkdirSync(Paths.LOCAL_DATA);
        }
    }
    async disconnect() {
        this.client.kill();
        await new Promise(resolve => setTimeout(resolve, 1000));
    }
    async trackDevices() {
        const tracker = await this.client.trackDevices();
        tracker.on("add", (device) => {
            log_1.log.info("Device %s was plugged in", device.id);
            this.syncDevices();
        });
        tracker.on("remove", (device) => {
            log_1.log.info("Device %s was unplugged", device.id);
            this.syncDevices();
        });
        tracker.on("end", (device) => {
            log_1.log.info("Tracking stopped");
            setTimeout(this.syncDevices.bind(this), 2000);
        });
        await this.syncDevices();
        return tracker;
    }
    getDevice(deviceId) {
        try {
            return this.requireDevice(deviceId);
        }
        catch (e) {
            log_1.log.info(e.message);
            return null;
        }
    }
    requireDevice(deviceId) {
        if (this.devices.length < 1) {
            throw new Error("No devices are connected.");
        }
        if (deviceId === AdbClient.FIRST_DEVICE) {
            const result = this.devices[0];
            if (!result)
                throw new Error("Bad first device.");
            return result;
        }
        if (parseInt(deviceId).toString() === deviceId) {
            const index = parseInt(deviceId);
            if (index < 0 || index >= this.devices.length) {
                throw new Error("Device " + index + " is out of range (" + this.devices.length + ")");
            }
            const result = this.devices[index];
            if (!result)
                throw new Error("Non device found at index " + deviceId);
            return result;
        }
        const result = this.devices.find((x) => x.id === deviceId);
        if (!result)
            throw new Error("No device found with id " + deviceId);
        return result;
    }
    async loadDevice(deviceId) {
        await this.syncDevices();
        return this.requireDevice(deviceId);
    }
    async getDevices() {
        await this.syncDevices();
        return this.devices;
    }
    async getVersion() {
        return await this.client.version();
    }
    async syncDevices() {
        const deviceDatas = await this.client.listDevices();
        let changed = false;
        // check added
        for (const deviceData of deviceDatas) {
            const existing = this.getDevice(deviceData.id);
            if (!existing) {
                changed = true;
                const device = await this.wrapDevice(deviceData);
                this.devices.push(device);
                log_1.log.info("device " + device.id + " added");
                if (typeof this.onDeviceConnected === "function") {
                    this.onDeviceConnected(device);
                }
            }
        }
        // check removed
        for (let i = this.devices.length - 1; i >= 0; i--) {
            const device = this.devices[i];
            const existing = deviceDatas.find((x) => x.id == device.id);
            if (!existing) {
                changed = true;
                log_1.log.info("device " + device.id + " removed");
                this.devices.splice(i, 1);
                if (typeof this.onDeviceDisconnected === "function") {
                    this.onDeviceConnected(device);
                }
            }
        }
        // dispatch if changed
        if (changed && typeof this.onDevicesChanged === "function") {
            this.onDevicesChanged(this.devices);
        }
    }
    async wrapDevice(deviceData) {
        let modelStr = await this.client.shell(deviceData.id, "getprop ro.product.model").then(require("adbkit").util.readAll);
        if (Boolean(modelStr)) {
            modelStr = modelStr.toString().toUpperCase();
        }
        return new AdbDevice_1.AdbDevice(this.client, modelStr, deviceData);
    }
}
exports.default = AdbClient;
AdbClient.FIRST_DEVICE = "first";
