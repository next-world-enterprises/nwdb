import { Client, ClientDeviceInfo } from "./Adbkit";
import { DeviceModel } from "./DeviceModel";
export interface DeviceExperience {
    appId: string;
    productName: string;
    productImage?: string;
    companyName?: string;
    version: string;
    modified?: string;
    engineVersion?: string;
    href?: string;
    activity?: string;
}
export interface DeviceCatalog {
    experiences: DeviceExperience[];
}
export declare class AdbDevice {
    client: Client;
    deviceModelStr: string;
    id: string;
    type: string;
    model: DeviceModel;
    static getDeviceModelType(deviceModelStr: string): ({
        new (device: AdbDevice): DeviceModel;
    } | null);
    constructor(client: Client, deviceModelStr: string, deviceData: ClientDeviceInfo);
    getAppData(): Promise<any>;
    downloadAppLog(): Promise<void>;
    installFromCloud(packageName: string): Promise<void>;
    update(): Promise<void>;
    reboot(): Promise<any>;
    searchForElement(elementId: string, maxTimesToSearch: number, tap?: boolean, linesToSkip?: number): Promise<boolean>;
    pressButton(buttonId: string, continueIfNotFound?: boolean, tap?: boolean): Promise<boolean>;
    getUI(): Promise<Document>;
    getPosition(buttonBounds: any): [number, number];
    tapExtDisplay(x: number, y: number): Promise<void>;
    pushFile(localFilePath: string, destination: string, fileName: string, showPercent?: boolean, descriptionOfTransfer?: string, percentUpdateFrequency?: number): Promise<string>;
    setVolume(percentage: number): Promise<void>;
    wait(ms?: number): Promise<void>;
    adbRun(...args: string[]): Promise<string>;
    shell(command: string): Promise<string>;
    getPackageNames(matchFilter: string): Promise<string[]>;
    getPacksDownloaded(): Promise<string[]>;
    getPacksInstalled(): Promise<string[]>;
    readFile(filename: string): Promise<string>;
    getNextworldId(): Promise<string>;
    getSerial(): Promise<string>;
    writeFile(filename: string, data: string): Promise<string>;
    readCatalog(): Promise<DeviceCatalog>;
    writeCatalog(catalog: DeviceCatalog): Promise<void>;
    getQueuedEvents(packageName: string): Promise<string>;
    getAllEvents(packageName: string): Promise<string>;
    startApp(packageName: string): Promise<void>;
    uninstall(packageName: string): Promise<void>;
    setEnv(env: string): Promise<void>;
    isInstalled(packageName: string): Promise<boolean>;
    isCatalogued(packageName: string): Promise<boolean>;
    installApk(apkPath: string, packageName: string): Promise<{
        packageName: string;
        title: string;
        launchActivity: string;
    }>;
}
