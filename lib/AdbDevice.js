"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdbDevice = void 0;
const Adbkit_1 = require("./Adbkit");
const FS = __importStar(require("fs-extra"));
const OS = __importStar(require("os"));
const Https = __importStar(require("https"));
const utils_1 = require("./utils");
const ChildProcess = __importStar(require("child_process"));
const log_1 = require("./log");
const Paths = __importStar(require("./paths"));
const xmldom_qsa_1 = require("xmldom-qsa");
const DeviceModel_1 = require("./DeviceModel");
const PicoDevice_1 = require("./devices/PicoDevice");
class AdbDevice {
    constructor(client, deviceModelStr, deviceData) {
        this.client = client;
        this.deviceModelStr = deviceModelStr;
        this.id = "";
        this.type = "";
        this.id = deviceData.id;
        this.type = deviceData.type;
        const deviceType = AdbDevice.getDeviceModelType(deviceModelStr);
        this.model = new deviceType(this);
    }
    static getDeviceModelType(deviceModelStr) {
        let deviceModelId = DeviceModel_1.DEVICE_MODEL_ID.Unknown;
        for (const key of Object.keys(DeviceModel_1.DEVICE_MODEL_ID)) {
            if (deviceModelStr.startsWith(DeviceModel_1.DEVICE_MODEL_ID[key])) {
                deviceModelId = DeviceModel_1.DEVICE_MODEL_ID[key];
            }
        }
        switch (deviceModelId) {
            case DeviceModel_1.DEVICE_MODEL_ID.Pico:
                return PicoDevice_1.PicoDeviceModel;
        }
        return DeviceModel_1.DeviceModel;
    }
    async getAppData() {
        await this.downloadAppLog();
        if (FS.existsSync(Paths.LOCAL_APPS) === false) {
            throw new Error("App index is not available.");
        }
        const appDataStr = FS.readFileSync(Paths.LOCAL_APPS, {
            encoding: "utf-8",
        });
        const appData = JSON.parse(appDataStr);
        return appData;
    }
    downloadAppLog() {
        return new Promise((resolve, reject) => {
            const file = FS.createWriteStream(Paths.LOCAL_APPS);
            const request = Https.get(Paths.URL_APPS_JSON, response => {
                response.pipe(file);
                file.on("finish", () => {
                    file.close();
                    resolve();
                });
            });
            request.on("error", reject);
        });
    }
    async installFromCloud(packageName) {
        let apkPath;
        const appData = await this.getAppData();
        const isAgent = packageName === appData.agent.package;
        if (isAgent) {
            apkPath = appData.agent.apk;
        }
        else {
            const game = appData.games.find((x) => x.package === packageName);
            if (!game) {
                throw new Error("Package '" + packageName + "' is not declared.");
            }
            apkPath = game.apk;
        }
        const agentIsInstalled = await this.isInstalled(appData.agent.package);
        if (agentIsInstalled === false) {
            await this.update();
        }
        await this.installApk(apkPath, packageName);
        log_1.log.info(packageName + " installed");
        if (isAgent) {
            await this.reboot();
        }
    }
    async update() {
        const appData = await this.getAppData();
        await this.installApk(appData.agent.apk, appData.agent.package);
        log_1.log.info("Nextworld XR agent installed");
    }
    async reboot() {
        return await this.client.reboot(this.id);
    }
    async searchForElement(elementId, maxTimesToSearch, tap = true, linesToSkip = 4) {
        let foundTheElement = false;
        let count = 0;
        while (foundTheElement == false && count < maxTimesToSearch) {
            foundTheElement = await this.pressButton(elementId, true, tap);
            if (foundTheElement == false) {
                for (let i = 0; i < linesToSkip; ++i) {
                    await this.shell("input keyevent 'KEYCODE_DPAD_DOWN'");
                }
                count++;
            }
        }
        return foundTheElement ? true : false;
    }
    async pressButton(buttonId, continueIfNotFound = false, tap = true) {
        var pos = -1;
        var nodeNum = -1;
        var retryCount = 0;
        buttonId = buttonId.toLowerCase();
        while (pos == -1 && retryCount < 3) {
            const ui = await this.getUI();
            for (var i = 0; i < ui.querySelectorAll("node").length; ++i) {
                let node = ui.querySelectorAll("node")[i];
                let nodeAttr0Name = node.attributes["0"].name;
                let nodeAttr1Value = node.attributes["1"].value.toLowerCase();
                let nodeAttr2Value = node.attributes["2"].value.toLowerCase();
                let nodeAttr3Value = node.attributes["3"].value.toLowerCase();
                if (nodeAttr0Name == "index") {
                    if (nodeAttr1Value.includes(buttonId) || nodeAttr2Value.includes(buttonId) || nodeAttr3Value.includes(buttonId)) {
                        pos = 16;
                        nodeNum = i;
                        break;
                    }
                }
                else if (nodeAttr0Name == "NAF" && nodeAttr3Value.includes(buttonId)) {
                    pos = 17;
                    nodeNum = i;
                    break;
                }
            }
            if (pos > -1) {
                const buttonBounds = ui.querySelectorAll("node")[nodeNum].attributes[pos.toString()].value;
                const [x, y] = this.getPosition(buttonBounds);
                if (tap == true) {
                    await this.tapExtDisplay(x, y);
                }
                else {
                    await this.shell("input press " + x + " " + y);
                }
            }
            else {
                if (continueIfNotFound == false) {
                    console.error(`Button "${buttonId}" could not be found. Trying again.\n`);
                    retryCount++;
                }
                break;
            }
        }
        if (pos == -1) {
            if (continueIfNotFound) {
                return false;
            }
            else {
                throw new Error(`"${buttonId}" is not a valid button on the current screen.`);
            }
        }
        return true;
    }
    async getUI() {
        const uiStr = await this.adbRun("exec-out uiautomator dump /dev/tty");
        const parser = new xmldom_qsa_1.DOMParser();
        let document = null;
        try {
            document = parser.parseFromString(uiStr, "text/xml");
        }
        catch (e) { }
        if (!document) {
            let msg = "UI XML could not be read";
            if (log_1.log.isVerbose) {
                msg += ":\n" + uiStr;
            }
            throw new Error(msg);
        }
        return document;
    }
    getPosition(buttonBounds) {
        if (!buttonBounds)
            throw new Error(`Cannot get bounds of buttonBounds: ${buttonBounds}`);
        const values = buttonBounds.replace("[", "").replace("][", ",").replace("]", "").split(",").map((bounds) => parseInt(bounds));
        const x = values[0];
        const y = values[1];
        const w = values[2];
        const h = values[3];
        const px = Math.round((x + w) * 0.5);
        const py = Math.round((y + h) * 0.5);
        return [px, py];
    }
    async tapExtDisplay(x, y) {
        await this.shell("input --ext-display tap " + x + " " + y + "&> /dev/null");
    }
    async pushFile(localFilePath, destination, fileName, showPercent = false, descriptionOfTransfer = "", percentUpdateFrequency = 10) {
        if (FS.existsSync(localFilePath) === false) {
            throw new Error(`File: ${localFilePath} doesn't exist.`);
        }
        var fileSize = (await FS.stat(localFilePath)).size;
        var percentComplete = 0;
        var lastPercentageShown = -1;
        return new Promise((resolve, reject) => {
            this.client.push(this.id, localFilePath, destination + fileName).then(transfer => {
                transfer.on("progress", stats => {
                    if (showPercent && descriptionOfTransfer != "") {
                        percentComplete = Math.round(((stats.bytesTransferred == undefined ? 0 : stats.bytesTransferred) / fileSize) * 100);
                        if (percentComplete % percentUpdateFrequency == 0 && percentComplete > lastPercentageShown) {
                            lastPercentageShown = percentComplete;
                            log_1.log.output(`${percentComplete}% transferred ${descriptionOfTransfer} to device.`);
                        }
                    }
                });
                transfer.on("end", () => {
                    if (descriptionOfTransfer != "")
                        log_1.log.output(`\n${descriptionOfTransfer} has been transferred to the device.\n`);
                    resolve(this.id);
                });
                transfer.on("error", e => {
                    console.error("Transfer error", e);
                    reject(e);
                });
            })
                .catch(reject);
        });
    }
    async setVolume(percentage) {
        // The audio service call takes a number between 0 and 15 (inclusive) for the volume level so we need to convert the percentage to an acceptable number.
        // Sanitise the percentage first.
        percentage = percentage > 100 ? 100 : (percentage < 0 ? 0 : percentage);
        const value = Math.round((percentage / 100) * 15);
        await this.shell("service call audio 10 i32 3 i32 " + value.toString() + " i32 1");
        await this.wait();
        log_1.log.info(`Audio level on device has been set to '${percentage}%'.`);
    }
    async wait(ms = 500) {
        await new Promise(resolve => setTimeout(resolve, ms));
    }
    adbRun(...args) {
        if (args.length === 1) {
            args = args[0].split(" ");
        }
        return new Promise((resolve, reject) => {
            let stdout = "";
            let handled = false;
            function handleClose(code) {
                if (handled)
                    return;
                handled = true;
                if (code !== 0) {
                    reject("ADB exited with " + code);
                    return;
                }
                resolve(stdout);
            }
            const cp = ChildProcess.spawn((0, utils_1.getBinPath)(), [
                "-s", this.id,
                ...args
            ]);
            process.stdin.pipe(cp.stdin);
            cp.stderr.pipe(process.stderr);
            cp.stdout.on("data", s => { stdout += s; });
            cp.on("exit", handleClose);
            cp.on("close", handleClose);
            cp.on("disconnect", handleClose);
            cp.on("error", handleClose);
        });
    }
    shell(command) {
        return this.adbRun("shell " + command);
    }
    async getPackageNames(matchFilter) {
        const allPackages = await this.client.getPackages(this.id);
        let result = allPackages;
        if (matchFilter) {
            result = result.filter(x => x.match(matchFilter));
        }
        return result;
    }
    async getPacksDownloaded() {
        if ((await this.isInstalled(Paths.PACKAGE_HOME)) === false) {
            throw new Error("Home is not installed.");
        }
        const output = await this.shell("find " + Paths.DEVICE_HOME_FILES + " -type f -name \"*.nwpz\"");
        log_1.log.info("output", output);
        const lines = output.trim().split("\n").filter(x => Boolean(x));
        const packs = lines.map(x => x.substring(x.lastIndexOf("/") + 1));
        log_1.log.info("packs", packs.join(", "));
        return packs;
    }
    async getPacksInstalled() {
        const output = await this.shell("find " + Paths.DEVICE_HOME_PACKS + " -type d");
        log_1.log.info("output", output);
        const lines = output.trim().split("\n").filter(x => Boolean(x));
        const packs = lines.map(x => x.substring(x.lastIndexOf("/") + 1));
        log_1.log.info("packs", packs.join(", "));
        return packs;
    }
    readFile(filename) {
        filename = Paths.parseDevicePath(filename);
        return new Promise((resolve, reject) => {
            this.client.pull(this.id, filename)
                .then(x => (0, utils_1.readDeviceStream)(x))
                .then(resolve)
                .catch(reject);
        });
    }
    async getNextworldId() {
        let deviceId = await this.readFile(Paths.DEVICE_AGENT_FILES + "/deviceId.data");
        if (!deviceId) {
            throw new Error("No Nextworld Id");
        }
        deviceId = deviceId.trim();
        if (!deviceId) {
            throw new Error("No Nextworld Id");
        }
        return deviceId;
    }
    async getSerial() {
        return this.id;
    }
    writeFile(filename, data) {
        filename = Paths.parseDevicePath(filename);
        return new Promise((resolve, reject) => {
            const tempPath = Paths.parse(OS.tmpdir(), "nwdb-" + Math.random().toString().substring(2) + ".txt");
            FS.writeFileSync(tempPath, data);
            const cleanup = () => {
                try {
                    FS.unlinkSync(tempPath);
                }
                catch (e) { }
            };
            let result = "";
            this.client.push(this.id, tempPath, filename).then(transfer => {
                transfer.on("data", chunk => result += chunk);
                transfer.on("end", () => {
                    cleanup();
                    log_1.log.info("finished writing " + filename);
                    resolve(result);
                });
                transfer.on("error", e => {
                    cleanup();
                    reject(e);
                });
            });
        });
    }
    async readCatalog() {
        let catalog = null;
        try {
            const catalogStr = await this.readFile(Paths.DEVICE_AGENT_CATALOG);
            catalog = JSON.parse(catalogStr);
        }
        catch (e) { }
        if (!catalog) {
            catalog = {
                experiences: []
            };
        }
        if (catalog && catalog.experiences.length > 0) {
            for (let i = 0; i < catalog.experiences.length; i++) {
                const exp = catalog.experiences[i];
                const isInstalled = await this.isInstalled(exp.appId);
                if (isInstalled == false) {
                    catalog.experiences.splice(i, 1);
                }
            }
        }
        return catalog;
    }
    async writeCatalog(catalog) {
        await this.writeFile(Paths.DEVICE_AGENT_CATALOG, JSON.stringify(catalog));
    }
    async getQueuedEvents(packageName) {
        const filename = Paths.parseDevicePath(Paths.DEVICE_DATA, packageName, "files/nw-queue.log");
        const events = await this.readFile(filename);
        return events;
    }
    async getAllEvents(packageName) {
        const filename = Paths.parseDevicePath(Paths.DEVICE_DATA, packageName, "files/nw-events.log");
        const events = await this.readFile(filename);
        return events;
    }
    async startApp(packageName) {
        const catalog = await this.readCatalog();
        const deviceApp = catalog.experiences.find(x => x.appId === packageName);
        if (!deviceApp) {
            throw new Error("Package " + packageName + " is not in catalog.");
        }
        if (!deviceApp.activity) {
            throw new Error("Package " + packageName + " has no launch activity.");
        }
        await this.shell("am start -n " + deviceApp.appId + "/" + deviceApp.activity);
    }
    async uninstall(packageName) {
        try {
            await this.client.uninstall(this.id, packageName);
        }
        catch (e) {
            throw new Error("Could not uninstall package " + packageName + ": " + e);
        }
    }
    async setEnv(env) {
        const settingsPath = Paths.DEVICE_AGENT_FILES + "/nw-settings.ini";
        let settingsStr = "";
        try {
            settingsStr = await this.readFile(settingsPath);
        }
        catch (e) { }
    }
    async isInstalled(packageName) {
        return await this.client.isInstalled(this.id, packageName);
    }
    async isCatalogued(packageName) {
        const isInstalled = await this.isInstalled(packageName);
        if (isInstalled === false)
            return false;
        const catalog = await this.readCatalog();
        return Object.values(catalog).findIndex(x => x.packageName === packageName) >= 0;
    }
    async installApk(apkPath, packageName) {
        if (packageName) {
            if (packageName.toLowerCase().startsWith("com.nextworld.") === false)
                throw new Error("Package is not a Nextworld XR Experience.");
        }
        apkPath = (0, utils_1.absPath)(apkPath);
        if (FS.existsSync(apkPath) === false)
            throw new Error("Package '" + apkPath + "' file not found.");
        if (apkPath.toLowerCase().endsWith(".apk") === false)
            throw new Error("Package must be an APK");
        const manifest = await (0, Adbkit_1.readManifest)(apkPath);
        if (!manifest) {
            throw new Error("Couldn't read manifest for " + apkPath);
        }
        const launchActivity = manifest.application?.launcherActivities?.[0] ||
            manifest.application?.activities?.find(x => x.intentFilters?.find(y => y.categories?.find(z => z.name.includes("LAUNCHER")))) ||
            manifest.application?.activities[0];
        if (!launchActivity) {
            throw new Error("Couldn't find launch activity in package " + manifest.package);
        }
        log_1.log.info("installing " + Paths.basename(apkPath) + "...");
        try {
            await this.client.install(this.id, apkPath);
        }
        catch (e) {
            throw new Error("Could not install " + apkPath + ": " + e);
        }
        const result = {
            packageName: manifest.package,
            title: launchActivity.label,
            launchActivity: launchActivity.name
        };
        const catalog = await this.readCatalog();
        catalog.experiences = catalog.experiences.filter(x => x.appId != manifest.package);
        catalog.experiences.push({
            appId: manifest.package,
            productName: launchActivity.label,
            version: manifest.versionName,
            companyName: manifest.package.split(".")?.[1] || "",
            activity: launchActivity.name
        });
        await this.writeCatalog(catalog);
        return result;
    }
}
exports.AdbDevice = AdbDevice;
