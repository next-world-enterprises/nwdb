/// <reference types="node" />
import { Stream } from "stream";
export interface ClientDeviceInfo {
    id: string;
    type: string;
}
export declare type DeviceListener = (event: string, callback: (device: ClientDeviceInfo) => void) => void;
export interface ClientTracker {
    on: DeviceListener;
}
export interface ClientTransferStats {
    bytesTransferred: number;
}
export declare type ClientEncoding = "utf-8";
export interface ClientTransfer extends Stream {
    on(eventName: "progress", callback: (stats: ClientTransferStats) => void): this;
    on(eventName: "end", callback: () => void): this;
    on(eventName: "data", callback: (chunk: string) => void): this;
    on(eventName: "error", callback: (error: string) => void): this;
}
export interface Client {
    listDevices(): Promise<ClientDeviceInfo[]>;
    trackDevices(): Promise<ClientTracker>;
    install(deviceId: string, apkPath: string): void;
    uninstall(deviceId: string, packageName: string): void;
    getPackages(deviceId: string): Promise<string[]>;
    isInstalled(deviceId: string, packageName: string): Promise<boolean>;
    version(): Promise<string>;
    pull(deviceId: string, filename: string): Promise<ClientTransfer>;
    push(deviceId: string, filename: string, devicePath: string): Promise<ClientTransfer>;
    kill(cb?: (() => void)): Promise<any>;
    reboot(deviceId: string, cb?: (() => void)): Promise<any>;
    shell(deviceId: string, command: string, cb?: (() => void)): Promise<any>;
}
export declare function createClient(options: any): Client;
export declare function readManifest(apkPath: string): Promise<IManifest>;
declare const Adb: {
    createClient: typeof createClient;
};
interface IManifestEntry {
    name: string;
}
interface IManifestActivity extends IManifestEntry {
    label: string;
    intentFilters: {
        actions: IManifestEntry[];
        categories: IManifestEntry[];
    }[];
}
interface IManifest {
    versionName: string;
    package: string;
    application: {
        label: string;
        activities: IManifestActivity[];
        launcherActivities: IManifestActivity[];
    };
}
export default Adb;
