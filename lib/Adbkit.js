"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.readManifest = exports.createClient = void 0;
function createClient(options) {
    return require("adbkit").createClient(options);
}
exports.createClient = createClient;
function readManifest(apkPath) {
    return new Promise((resolve, reject) => {
        const ApkReader = require("adbkit-apkreader");
        ApkReader.open(apkPath)
            .then((x) => x.readManifest())
            .then(resolve)
            .catch(reject);
    });
}
exports.readManifest = readManifest;
const Adb = {
    createClient
};
exports.default = Adb;
