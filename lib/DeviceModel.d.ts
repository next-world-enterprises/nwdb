import { AdbDevice } from "./AdbDevice";
export declare const DEVICE_MODEL_ID: {
    readonly Oculus: "OCULUS";
    readonly Pico: "PICO NEO";
    readonly Unknown: "UNKNOWN";
};
export declare type DeviceModelId = typeof DEVICE_MODEL_ID[keyof typeof DEVICE_MODEL_ID];
export declare class DeviceModel {
    adbDevice: AdbDevice;
    device: AdbDevice;
    deviceModel: DeviceModel;
    constructor(adbDevice: AdbDevice);
    updateOS(): Promise<void>;
    downloadOS(): Promise<void>;
    osKiosk(enableKiosk: boolean): Promise<void>;
    getLatestOSVersion(): Promise<string>;
    nwEnvSettings(): Promise<void>;
    setProps(doLog?: boolean): Promise<void>;
    setDefaultPackage(packageName: string, withGuardian: boolean): Promise<void>;
}
