"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeviceModel = exports.DEVICE_MODEL_ID = void 0;
exports.DEVICE_MODEL_ID = {
    Oculus: "OCULUS",
    Pico: "PICO NEO",
    Unknown: "UNKNOWN"
};
class DeviceModel {
    constructor(adbDevice) {
        this.adbDevice = adbDevice;
        this.device = null;
        this.deviceModel = null;
        this.device = adbDevice;
    }
    async updateOS() {
        throw new Error("Upgrading the device OS is not yet possible for this device type.");
    }
    async downloadOS() {
        throw new Error("Downloading the latest OS version is not yet possible for this device type.");
    }
    async osKiosk(enableKiosk) {
        throw new Error("Enabling/Disabling kiosk mode is not yet possible for this device type.");
    }
    async getLatestOSVersion() {
        throw new Error("Getting the latest OS version string is not yet possible for this device type.");
    }
    async nwEnvSettings() {
        console.log(this.deviceModel);
        throw new Error("Setting Next World environment settings is not yet possible on this device type.");
    }
    async setProps(doLog = true) {
        throw new Error("Setting props is not yet possible on this device type.");
    }
    async setDefaultPackage(packageName, withGuardian) {
        throw new Error("Setting Agent as default package is not yet possible on this device type.");
    }
}
exports.DeviceModel = DeviceModel;
