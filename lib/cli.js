"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const process_1 = require("process");
const CommandList = __importStar(require("./commands"));
const log_1 = require("./log");
const TITLE = "NWCommander CLI";
const cliOptions = { ...CommandList.DEFAULT_OPTIONS };
const optionMap = [
    ["device", "d", "set the device serial or index (default 0)", String],
    ["verbose", "v", "sets the log output to verbose", Boolean]
];
function getCommand(name) {
    const commands = CommandList.getCommands();
    const cmd = commands.find(x => x.name.trim().toLowerCase() == name.trim().toLowerCase());
    return cmd;
}
function printHelpAll() {
    log_1.log.help("How to use:");
    const commands = CommandList.getCommands();
    for (const cmd of commands) {
        log_1.log.help("  " + cmd.name.padEnd(17) + cmd.description);
        if (cliOptions.verbose) {
            printArgs(cmd.arguments, "    ");
            log_1.log.help("");
        }
    }
    log_1.log.help("Options:");
    for (const option of optionMap) {
        log_1.log.help("  --" + (option[0] + " | -" + option[1]).padEnd(15) + option[2]);
    }
    process.exit(1);
}
function printArgs(argDescriptions, indent = "") {
    if (argDescriptions) {
        for (const x of argDescriptions) {
            log_1.log.help(indent + "  " + ("[" + x[0] + "]").padEnd(20) + x[1]);
        }
    }
}
function printHelpOne(methodName) {
    const cmd = getCommand(methodName);
    if (!cmd) {
        log_1.log.error("No command '" + methodName + "'");
        printHelpAll();
        return;
    }
    log_1.log.help(cmd.name + ": " + cmd.description);
    const argStr = cmd.arguments ? cmd.arguments.map(x => "[" + x[0] + "]").join("") : "";
    log_1.log.help("  i.e.: nwdb " + methodName + " " + argStr);
    if (cmd.arguments) {
        log_1.log.help("arguments:");
        printArgs(cmd.arguments);
    }
}
function printHelpMethods() {
    const commands = CommandList.getCommands();
    log_1.log.help("  methods are: " + commands.map(x => x.name).join("|") + "\n('nwdb help' for more info)");
}
async function run() {
    process.on("uncaughtException", e => {
        log_1.log.error(e);
        process.exit(1);
    });
    const args = process.argv.slice(2);
    for (let i = 0; i < args.length; i++) {
        let key = args[i].trim();
        if (key.charAt(0) !== '-')
            continue;
        if (!key)
            continue;
        key = key.toLowerCase().trim();
        while (key.charAt(0) === '-') {
            key = key.substring(1);
        }
        const value = (args[i + 1] || "").trim();
        let spliceCount = 2;
        for (const [name, shortName, description, objType] of optionMap) {
            if (name === key || shortName === key) {
                cliOptions[name] = value;
                if (objType === Boolean) {
                    spliceCount = 1;
                    cliOptions[name] = "1";
                }
            }
        }
        args.splice(i, spliceCount);
    }
    const methodName = args.shift();
    if (!methodName || methodName === "help") {
        log_1.log.info(TITLE);
        if (args.length > 0) {
            printHelpOne(args[0]);
        }
        else {
            printHelpAll();
        }
        return;
    }
    const cmd = getCommand(methodName);
    (0, log_1.setLogLevel)(Boolean(cliOptions.verbose));
    if (cliOptions.verbose) {
        log_1.log.info("verbose logging...");
    }
    if (cmd) {
        try {
            cmd.verifyArgs(...args);
        }
        catch (e) {
            log_1.log.error(e.message);
            printHelpOne(methodName);
            process.exit(1);
        }
        try {
            cmd.setOptions(cliOptions);
            const result = await cmd.run(...args);
            if (result instanceof Promise) {
                log_1.log.error(new Error("Command needs awaiting."));
            }
            if (result instanceof Array) {
                log_1.log.output(result.map(x => String(x)).join("\n"));
            }
            else if (result !== undefined) {
                log_1.log.output(String(result));
            }
            if (result === false) {
                (0, process_1.exit)(1);
            }
        }
        catch (e) {
            const errorMsg = typeof e === "string" ? e : e instanceof Error ? cliOptions.verbose ? e.stack : e.message : "Unknown error";
            log_1.log.error(errorMsg);
            if (cliOptions.verbose) {
                printHelpOne(methodName);
            }
            process.exit(1);
        }
        if (log_1.log.hasOutput === false) {
            log_1.log.output("OK");
        }
        process.exit(0);
    }
    else {
        log_1.log.help(TITLE);
        if (methodName) {
            log_1.log.error("ERROR: Unknown method '" + methodName + "'");
            printHelpMethods();
        }
        else {
            log_1.log.error("ERROR: No method specified");
            printHelpMethods();
        }
        process.exit(1);
    }
}
if (module === require.main) {
    run();
}
module.exports = { CommandList, run };
