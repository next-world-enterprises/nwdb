export declare const DEFAULT_OPTIONS: {
    device: string;
    verbose: boolean;
};
export declare type CmdOptions = typeof DEFAULT_OPTIONS;
export declare type CmdOptionsKey = keyof CmdOptions;
export declare abstract class Cmd<T = string> {
    name: string;
    description: string;
    arguments?: string[][];
    options: CmdOptions;
    setOptions(options: Record<string, string>): void;
    verifyArgs(...args: string[]): void;
    run(...args: string[]): Promise<T>;
}
export declare class DeviceCmd extends Cmd<string[]> {
    name: string;
    description: string;
    run(): Promise<any>;
}
export declare class IsInstalledCmd extends Cmd<boolean> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<boolean>;
}
export declare class ListAppsAllCmd extends Cmd<string[]> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string[]>;
}
export declare class ListCatalogCmd extends Cmd<string[]> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string[]>;
}
export declare class VerifyCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class VersionCmd extends Cmd<string> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string>;
}
export declare class ListPacksDownloadedCmd extends Cmd<string[]> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string[]>;
}
export declare class ListPacksInstalledCmd extends Cmd<string[]> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string[]>;
}
export declare class UninstallCmd extends Cmd<void> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<void>;
}
export declare class InstallCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class ReadManifestCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class PrintCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class GetIdCmd extends Cmd<string> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string>;
}
export declare class UpgradeDeviceCmd extends Cmd<string> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string>;
}
export declare class SetNWEnvSettingsCmd extends Cmd<string> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string>;
}
export declare class SetKioskModeCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class StartAppCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class ShellCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class AdbCmd extends Cmd<string> {
    name: string;
    description: string;
    arguments: string[][];
    run(...args: string[]): Promise<string>;
}
export declare class DocsCmd extends Cmd<string> {
    name: string;
    description: string;
    run(...args: string[]): Promise<string>;
}
export declare function getCommands(): Cmd<any>[];
