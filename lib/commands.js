"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCommands = exports.DocsCmd = exports.AdbCmd = exports.ShellCmd = exports.StartAppCmd = exports.SetKioskModeCmd = exports.SetNWEnvSettingsCmd = exports.UpgradeDeviceCmd = exports.GetIdCmd = exports.PrintCmd = exports.ReadManifestCmd = exports.InstallCmd = exports.UninstallCmd = exports.ListPacksInstalledCmd = exports.ListPacksDownloadedCmd = exports.VersionCmd = exports.VerifyCmd = exports.ListCatalogCmd = exports.ListAppsAllCmd = exports.IsInstalledCmd = exports.DeviceCmd = exports.Cmd = exports.DEFAULT_OPTIONS = void 0;
const AdbClient_1 = __importDefault(require("./AdbClient"));
const Adbkit_1 = require("./Adbkit");
const FS = __importStar(require("fs"));
const events_1 = require("events");
events_1.EventEmitter.prototype.setMaxListeners(12);
exports.DEFAULT_OPTIONS = {
    device: "0",
    verbose: false
};
const commands = [];
class Cmd {
    constructor() {
        this.options = exports.DEFAULT_OPTIONS;
    }
    setOptions(options) {
        for (const key of Object.keys(options)) {
            this.options[key] = options[key];
        }
    }
    verifyArgs(...args) {
        if (this.arguments && this.arguments.length > 0) {
            for (let i = 0; i < this.arguments.length; i++) {
                if (!args[i] && this.arguments[i][2] === undefined) {
                    throw new Error("missing argument: please specify " + this.arguments[i][0]);
                }
            }
        }
    }
    async run(...args) {
        throw new Error("Not implemented.");
    }
}
exports.Cmd = Cmd;
class DeviceCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "devices";
        this.description = "get a list of attached devices";
    }
    async run() {
        const client = new AdbClient_1.default();
        const devices = await client.getDevices();
        return devices.map((x) => x.id);
    }
}
exports.DeviceCmd = DeviceCmd;
/*
// temporarily disabled due to undesirable traits
export class CloudInstallCmd extends Cmd<void> {
    name = "cloud-install";
    description = "installs an APK registered on S3";
    arguments = [["package name", "the package identifier to install"]];
    async run(...args: string[]) {
        const [packageName] = args;
        if (!packageName) throw new Error("packageName must be specified");
        const client = new AdbClient();
        const device = await client.loadDevice(this.options.device);
        await device.installFromCloud(packageName);
    }
}
*/
class IsInstalledCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "installed";
        this.description = "checks if a package is installed";
        this.arguments = [["package name", "the package identifier to install"]];
    }
    async run(...args) {
        const [packageName] = args;
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        const result = await device.isInstalled(packageName);
        return result;
    }
}
exports.IsInstalledCmd = IsInstalledCmd;
class ListAppsAllCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "lsapps";
        this.description = "get a list of all installed apps";
        this.arguments = [["filter", "a filter to search by", ""]];
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        const packageNames = await device.getPackageNames(args[0]);
        return packageNames;
    }
}
exports.ListAppsAllCmd = ListAppsAllCmd;
class ListCatalogCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "lscat";
        this.description = "get a list of catalogued apps";
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        const catalog = await device.readCatalog();
        if (this.options.verbose) {
            return catalog.experiences.map(x => JSON.stringify(x));
        }
        return Object.values(catalog.experiences).map(x => x.appId);
    }
}
exports.ListCatalogCmd = ListCatalogCmd;
class VerifyCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "verify";
        this.description = "returns an error if a package is NOT installed";
        this.arguments = [["package name", "the package identifier to verify"]];
    }
    async run(...args) {
        const [packageName] = args;
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        const result = await device.isInstalled(packageName);
        if (!result) {
            throw new Error("Package " + packageName + " is not installed.");
        }
        return "verified " + packageName;
    }
}
exports.VerifyCmd = VerifyCmd;
class VersionCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "version";
        this.description = "gets the version of adb";
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const version = await client.getVersion();
        return version;
    }
}
exports.VersionCmd = VersionCmd;
class ListPacksDownloadedCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "lspd";
        this.description = "lists the nextworld packs downloaded to the device";
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        return await device.getPacksDownloaded();
    }
}
exports.ListPacksDownloadedCmd = ListPacksDownloadedCmd;
class ListPacksInstalledCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "lspi";
        this.description = "lists the nextworld packs installed on the device";
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        return await device.getPacksInstalled();
    }
}
exports.ListPacksInstalledCmd = ListPacksInstalledCmd;
class UninstallCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "uninstall";
        this.description = "uninstalls an apk";
        this.arguments = [["package name", "the package identifier to uninstall"]];
    }
    async run(...args) {
        const [packageName] = args;
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        await device.uninstall(packageName);
    }
}
exports.UninstallCmd = UninstallCmd;
class InstallCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "install";
        this.description = "installs an apk";
        this.arguments = [["apk path", "the local path to the apk to be installed"]];
    }
    async run(...args) {
        const [filePath, packageName] = args;
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        const result = await device.installApk(filePath, packageName);
        if (!result)
            throw new Error("No result.");
        return "Installed " + result.packageName;
    }
}
exports.InstallCmd = InstallCmd;
/*
// temporarily disabled due to undesirable traits
export class UpdateCmd extends Cmd<void> {
    name = "update";
    description = "updates the agent";
    async run(...args: string[]) {
        const client = new AdbClient();
        const device = await client.loadDevice(this.options.device);
        await device.update();
    }
}
*/
class ReadManifestCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "manifest";
        this.description = "reads a manifest";
        this.arguments = [["apk path", "the local path to the apk to read to inspect"]];
    }
    async run(...args) {
        const [apkPath] = args;
        const manifest = await (0, Adbkit_1.readManifest)(apkPath);
        return JSON.stringify(manifest, null, "  ");
    }
}
exports.ReadManifestCmd = ReadManifestCmd;
class PrintCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "print";
        this.description = "reads a text file";
        this.arguments = [["device path", "the device path for the text file to print"]];
    }
    async run(...args) {
        const [filePath] = args;
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        const text = await device.readFile(filePath);
        return text;
    }
}
exports.PrintCmd = PrintCmd;
class GetIdCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "nwid";
        this.description = "gets the nextworld id";
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        const text = await device.getNextworldId();
        return text;
    }
}
exports.GetIdCmd = GetIdCmd;
class UpgradeDeviceCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "os-upgrade";
        this.description = "upgrades the attached device to the latest version stored on nextworld infrastructure";
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        await device.model.updateOS();
        return "Your device is now upgrading and will restart automatically.";
    }
}
exports.UpgradeDeviceCmd = UpgradeDeviceCmd;
class SetNWEnvSettingsCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "os-nwenv";
        this.description = "sets the nextworld environment settings on the device";
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        await device.model.nwEnvSettings();
        return "The Next World Environment settings have been set up successfully.";
    }
}
exports.SetNWEnvSettingsCmd = SetNWEnvSettingsCmd;
class SetKioskModeCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "os-kiosk";
        this.description = "enable or disable kiosk mode for nextworld agent on the device";
        this.arguments = [["true/false", "true for enable, false for disable"]];
    }
    async run(...args) {
        let enableKioskMode;
        let sanitisedArgs = args[0].trim().toLowerCase();
        if (sanitisedArgs == "true" || sanitisedArgs == "false") {
            enableKioskMode = JSON.parse(sanitisedArgs);
        }
        else {
            throw new Error("Input was not true or false.");
        }
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        await device.model.osKiosk(enableKioskMode);
        return "Kiosk mode has been turned " + (enableKioskMode == true ? "on" : "off") + ".";
    }
}
exports.SetKioskModeCmd = SetKioskModeCmd;
class StartAppCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "start";
        this.description = "starts a catalogued app";
        this.arguments = [["package name", "the package identifier for the app to start"]];
    }
    async run(...args) {
        const [packageName] = args;
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        await device.startApp(packageName);
        return "started " + packageName;
    }
}
exports.StartAppCmd = StartAppCmd;
class ShellCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "shell";
        this.description = "forwards an adb shell command";
        this.arguments = [["...args", "the arguments to forward to shell"]];
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        return await device.shell(args.join(" "));
    }
}
exports.ShellCmd = ShellCmd;
class AdbCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "adb";
        this.description = "forwards an adb command";
        this.arguments = [["...args", "the arguments to forward to adb"]];
    }
    async run(...args) {
        const client = new AdbClient_1.default();
        const device = await client.loadDevice(this.options.device);
        return await device.adbRun(...args);
    }
}
exports.AdbCmd = AdbCmd;
class DocsCmd extends Cmd {
    constructor() {
        super(...arguments);
        this.name = "docs";
        this.description = "generates the README.md for this CLI";
    }
    async run(...args) {
        const commands = getCommands();
        const packageJson = JSON.parse(FS.readFileSync(__dirname + "/../package.json"));
        let str = "# " + packageJson.description + "\n\n## Commands:\n\n";
        for (const cmd of commands) {
            str += "### " + cmd.name + "\n" + cmd.description + "\n\n";
            str += "> `nwdb " + cmd.name;
            if (cmd.arguments) {
                str += " " + cmd.arguments?.map(x => "[" + x[0] + "]").join(" ");
            }
            str += "`\n";
            if (cmd.arguments) {
                for (const arg of cmd.arguments) {
                    str += "  - **[" + arg[0] + "]**: " + arg[1] + "\n";
                }
            }
            str += "\n\n";
        }
        FS.writeFileSync(__dirname + "/../README.md", str);
        return "docs generated";
    }
}
exports.DocsCmd = DocsCmd;
function getCommands() {
    if (commands.length < 1) {
        for (const key in module.exports) {
            if (module.exports.hasOwnProperty(key) === false)
                continue;
            const CmdClass = module.exports[key];
            if (CmdClass && CmdClass.constructor && CmdClass.constructor.name && CmdClass.constructor === Function && CmdClass !== Cmd) {
                const cmd = new CmdClass();
                if (cmd && cmd instanceof Cmd) {
                    commands.push(cmd);
                }
            }
        }
    }
    return commands;
}
exports.getCommands = getCommands;
