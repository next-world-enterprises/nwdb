import { AdbDevice } from "../AdbDevice";
import { DeviceModel } from "../DeviceModel";
export declare class PicoDeviceModel extends DeviceModel {
    device: AdbDevice;
    constructor(device: AdbDevice);
    updateOS(): Promise<void>;
    downloadOS(): Promise<void>;
    getLatestOSVersion(): Promise<string>;
    nwEnvSettings(): Promise<void>;
    osKiosk(enableKiosk: boolean): Promise<void>;
    setProps(doLog?: boolean): Promise<void>;
    setSleepDelayNever(): Promise<void>;
    enableMiracasting(): Promise<void>;
    enableGlobalAntiDispersion(): Promise<void>;
    setDefaultPackage(packageName: string, withGuardian: boolean): Promise<void>;
}
