declare class Logger {
    isVerbose: boolean;
    hasOutput: boolean;
    info(...args: string[]): void;
    help(...args: string[]): void;
    warn(...args: string[]): void;
    error(first: string | Error, ...args: string[]): void;
    output(s: string): void;
    success(...args: string[]): void;
}
export declare const log: Logger;
export declare function setLogLevel(on: boolean): void;
export {};
