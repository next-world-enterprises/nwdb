"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setLogLevel = exports.log = void 0;
var StdOutColor;
(function (StdOutColor) {
    StdOutColor[StdOutColor["Default"] = 0] = "Default";
    StdOutColor[StdOutColor["Red"] = 31] = "Red";
    StdOutColor[StdOutColor["Green"] = 32] = "Green";
    StdOutColor[StdOutColor["Yellow"] = 33] = "Yellow";
})(StdOutColor || (StdOutColor = {}));
class Logger {
    constructor() {
        this.isVerbose = false;
        this.hasOutput = false;
    }
    info(...args) {
        this.isVerbose && consoleLog(args);
    }
    help(...args) {
        stdout(StdOutColor.Default, args);
    }
    warn(...args) {
        stdout(StdOutColor.Yellow, args);
    }
    error(first, ...args) {
        const errorStr = "ERROR: " + (first instanceof Error ? this.isVerbose ? first.stack : first.message : String(first) || "unknown") + args.join(" ");
        stdout(StdOutColor.Red, [errorStr]);
    }
    output(s) {
        this.hasOutput = true;
        stdout(StdOutColor.Default, [s]);
    }
    success(...args) {
        stdout(StdOutColor.Green, args);
    }
}
exports.log = new Logger();
function setLogLevel(on) {
    exports.log.isVerbose = on;
}
exports.setLogLevel = setLogLevel;
function consoleLog(args) {
    console.log(...args);
}
function stdout(color, args) {
    let str = "";
    if (color) {
        str += "\x1b[" + color + "m";
    }
    for (const arg of args) {
        str += arg + " ";
    }
    str += "\x1b[0m\n";
    process.stdout.write(str);
}
