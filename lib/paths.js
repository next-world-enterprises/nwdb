"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dirname = exports.basename = exports.parseDevicePath = exports.parse = exports.URL_APPS_JSON = exports.LOCAL_APPS = exports.LOCAL_DATA = exports.DEVICE_HOME_PACKS = exports.DEVICE_HOME_FILES = exports.DEVICE_AGENT_CATALOG = exports.DEVICE_AGENT_FILES = exports.DEVICE_DATA = exports.PACKAGE_HOME = exports.PACKAGE_AGENT = void 0;
const Path = __importStar(require("path"));
exports.PACKAGE_AGENT = "com.nextworld.agent";
exports.PACKAGE_HOME = "com.NextWorld.NextWorldMenu";
exports.DEVICE_DATA = "/sdcard/Android/data";
exports.DEVICE_AGENT_FILES = exports.DEVICE_DATA + "/" + exports.PACKAGE_AGENT + "/files";
exports.DEVICE_AGENT_CATALOG = exports.DEVICE_AGENT_FILES + "/catalog.data";
exports.DEVICE_HOME_FILES = exports.DEVICE_DATA + "/" + exports.PACKAGE_HOME + "/files";
exports.DEVICE_HOME_PACKS = exports.DEVICE_HOME_FILES + "/packs";
exports.LOCAL_DATA = Path.resolve(__dirname, "..", "data");
exports.LOCAL_APPS = Path.join(exports.LOCAL_DATA, "apps.json");
exports.URL_APPS_JSON = "https://app.nextworldenterprises.com/apps.json";
function parse(...pathParts) {
    if (pathParts.length > 0) {
        let s = Path.join(...pathParts).trim();
        s = s.replace(/\\/g, "//");
        s = s.replace(/\/+/g, "/");
        return s;
    }
    return null;
}
exports.parse = parse;
function parseDevicePath(...pathParts) {
    let s = parse(...pathParts);
    if (s.startsWith("/")) {
        s = s.substring(1);
    }
    if (s.startsWith("com.")) {
        s = exports.DEVICE_DATA + "/" + s;
    }
    return s;
}
exports.parseDevicePath = parseDevicePath;
function basename(path) {
    return Path.basename(path);
}
exports.basename = basename;
function dirname(path) {
    return Path.dirname(path);
}
exports.dirname = dirname;
