/// <reference types="node" />
import { Stream } from "stream";
export default class Utils {
    static httpsDownload(url: URL, purpose?: string, percentUpdateFrequency?: number, childFolder?: string): Promise<string>;
}
export declare function absPath(dataFile: string): string;
export declare function getBinPath(): string;
export declare function readDeviceStream(stream: Stream): Promise<string>;
