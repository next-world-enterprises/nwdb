"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.readDeviceStream = exports.getBinPath = exports.absPath = void 0;
const OS = __importStar(require("os"));
const Path = __importStar(require("path"));
const FS = __importStar(require("fs-extra"));
const Https = __importStar(require("https"));
const Paths = __importStar(require("./paths"));
const log_1 = require("./log");
class Utils {
    static httpsDownload(url, purpose = "", percentUpdateFrequency = 10, childFolder = "") {
        return new Promise((resolve, reject) => {
            const fileName = url.toString().substring(url.toString().lastIndexOf("/") + 1);
            // Check that the file has not previously been downloaded before hitting S3.
            FS.access(Path.join(Paths.LOCAL_DATA, childFolder, fileName), FS.constants.F_OK, (err) => {
                if (err === null) {
                    resolve(Path.join(Paths.LOCAL_DATA, childFolder, fileName));
                }
                else {
                    var totalLength = 0;
                    var downloadedLength = 0;
                    var percentComplete = 0;
                    var lastPercentageShown = -1;
                    const req = Https.request(url, (res) => {
                        totalLength = parseInt(res.headers["content-length"]);
                    });
                    req.end();
                    Https.get(url, (res) => {
                        const filePath = FS.createWriteStream(Path.join(Paths.LOCAL_DATA, childFolder, fileName));
                        res.pipe(filePath);
                        filePath.on("drain", () => {
                            downloadedLength = filePath.bytesWritten;
                            percentComplete = Math.round((downloadedLength / totalLength) * 100);
                            if (percentComplete % percentUpdateFrequency == 0 && percentComplete > lastPercentageShown) {
                                if (purpose != "")
                                    log_1.log.output(`${purpose} is ${percentComplete}% downloaded.`);
                                lastPercentageShown = percentComplete;
                            }
                        });
                        filePath.on("finish", () => {
                            filePath.close();
                            resolve(filePath.path);
                        });
                        filePath.on("error", err => {
                            FS.unlink(filePath.path);
                            reject("error: " + err.message);
                        });
                    });
                }
            });
        });
    }
}
exports.default = Utils;
function absPath(dataFile) {
    if (dataFile.startsWith("/") === false) {
        dataFile = Path.join(Path.dirname(__dirname), "data", dataFile);
    }
    return dataFile;
}
exports.absPath = absPath;
function getBinPath() {
    let binPath;
    const platform = OS.platform();
    if (platform === "win32") {
        binPath = Path.join(__dirname, "..", "bin", "win", "adb.exe");
    }
    else if (platform === "darwin") {
        binPath = Path.join(__dirname, "..", "bin", "mac", "adb");
    }
    return binPath;
}
exports.getBinPath = getBinPath;
function readDeviceStream(stream) {
    return new Promise((resolve, reject) => {
        let result = "";
        stream.on("data", chunk => result += chunk);
        stream.on("end", (e) => {
            resolve(result.trim());
        });
        stream.on("error", e => {
            reject(e);
        });
        stream.on("close", e => {
            resolve(result);
        });
    });
}
exports.readDeviceStream = readDeviceStream;
