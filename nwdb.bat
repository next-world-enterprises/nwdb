@echo off
cd "%~dp0"
IF exist node_modules ( echo. > nul ) ELSE ( npm install | goto :index ) 
:index
node index %*