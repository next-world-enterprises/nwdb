import Adb, { Client, ClientDeviceInfo } from "./Adbkit";
import * as FS from "fs";
import { getBinPath } from "./utils";
import { AdbDevice } from "./AdbDevice";
import { log } from "./log";
import * as Paths from "./paths";

export default class AdbClient {

	static FIRST_DEVICE = "first";

	client: Client | null = null;
	onDevicesChanged: (devices: AdbDevice[]) => void;
	onDeviceConnected: (device: AdbDevice) => void;
	onDeviceDisconnected: (device: AdbDevice) => void;
	devices: AdbDevice[] = [];

	constructor() {
		this.init();
	}

	async init() {
		this.client = Adb.createClient({ bin: getBinPath() });
		this.trackDevices();
		process.on("exit", () => {
			this.client.kill();
		});
		if (FS.existsSync(Paths.LOCAL_DATA) === false) {
			FS.mkdirSync(Paths.LOCAL_DATA);
		}
	}

	async disconnect() {
		this.client.kill();
		await new Promise(resolve => setTimeout(resolve, 1000));
	}

	async trackDevices() {
		const tracker = await this.client.trackDevices();
		tracker.on("add", (device) => {
			log.info("Device %s was plugged in", device.id);
			this.syncDevices();
		});
		tracker.on("remove", (device) => {
			log.info("Device %s was unplugged", device.id);
			this.syncDevices();
		});
		tracker.on("end", (device) => {
			log.info("Tracking stopped");
			setTimeout(this.syncDevices.bind(this), 2000);
		});
		await this.syncDevices();
		return tracker;
	}

	getDevice(deviceId: string) {
		try {
			return this.requireDevice(deviceId);
		} catch (e) {
			log.info(e.message);
			return null;
		}
	}

	requireDevice(deviceId: string) {
		if (this.devices.length < 1) {
			throw new Error("No devices are connected.");
		}
		if (deviceId === AdbClient.FIRST_DEVICE) {
			const result = this.devices[0];
			if (!result) throw new Error("Bad first device.");
			return result;
		}
		if (parseInt(deviceId).toString() === deviceId) {
			const index = parseInt(deviceId);
			if (index < 0 || index >= this.devices.length) {
				throw new Error("Device " + index + " is out of range (" + this.devices.length + ")");
			}
			const result = this.devices[index];
			if (!result) throw new Error("Non device found at index " + deviceId);
			return result;
		}
		const result = this.devices.find((x) => x.id === deviceId);
		if (!result) throw new Error("No device found with id " + deviceId);
		return result;
	}

	async loadDevice(deviceId: string) {
		await this.syncDevices();
		return this.requireDevice(deviceId);
	}

	async getDevices(): Promise<AdbDevice[]> {
		await this.syncDevices();
		return this.devices;
	}

	async getVersion() {
		return await this.client.version();
	}

	async syncDevices(): Promise<void> {
		const deviceDatas: ClientDeviceInfo[] = await this.client.listDevices();
		let changed = false;
		// check added
		for (const deviceData of deviceDatas) {
			const existing = this.getDevice(deviceData.id);
			if (!existing) {
				changed = true;
				const device = await this.wrapDevice(deviceData);
				this.devices.push(device);
				log.info("device " + device.id + " added");
				if (typeof this.onDeviceConnected === "function") {
					this.onDeviceConnected(device);
				}
			}
		}
		// check removed
		for (let i = this.devices.length - 1; i >= 0; i--) {
			const device = this.devices[i];
			const existing = deviceDatas.find((x) => x.id == device.id);
			if (!existing) {
				changed = true;
				log.info("device " + device.id + " removed");
				this.devices.splice(i, 1);
				if (typeof this.onDeviceDisconnected === "function") {
					this.onDeviceConnected(device);
				}
			}
		}
		// dispatch if changed
		if (changed && typeof this.onDevicesChanged === "function") {
			this.onDevicesChanged(this.devices);
		}
	}

	async wrapDevice(deviceData: ClientDeviceInfo) {
		let modelStr = await this.client.shell(deviceData.id, "getprop ro.product.model").then(require("adbkit").util.readAll);

		if (Boolean(modelStr)) {
			modelStr = modelStr.toString().toUpperCase();
		}

		return new AdbDevice(this.client, modelStr, deviceData);
	}
}
