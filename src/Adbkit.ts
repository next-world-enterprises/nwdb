import { Stream } from "stream";

export interface ClientDeviceInfo {
	id: string;
	type: string;
}

export type DeviceListener = (
	event: string,
	callback: (device: ClientDeviceInfo) => void
) => void;

export interface ClientTracker {
	on: DeviceListener;
}

export interface ClientTransferStats {
	bytesTransferred: number;
}

export type ClientEncoding = "utf-8";

export interface ClientTransfer extends Stream {
	on(eventName: "progress", callback: (stats: ClientTransferStats) => void): this;
	on(eventName: "end", callback: () => void): this;
	on(eventName: "data", callback: (chunk: string) => void): this;
	on(eventName: "error", callback: (error: string) => void): this;
}

export interface Client {
	listDevices(): Promise<ClientDeviceInfo[]>;
	trackDevices(): Promise<ClientTracker>;
	install(deviceId: string, apkPath: string): void;
	uninstall(deviceId: string, packageName: string): void;
	getPackages(deviceId: string): Promise<string[]>;
	isInstalled(deviceId: string, packageName: string): Promise<boolean>;
	version(): Promise<string>;
	pull(deviceId: string, filename: string): Promise<ClientTransfer>;
	push(deviceId: string, filename: string, devicePath: string): Promise<ClientTransfer>;
	kill(cb?: (() => void)): Promise<any>;
	reboot(deviceId: string, cb?: (() => void)): Promise<any>;
	shell(deviceId: string, command: string, cb?: (() => void)): Promise<any>;
}

export function createClient(options: any): Client {
	return require("adbkit").createClient(options);
}

export function readManifest(apkPath: string) {
	return new Promise<IManifest>((resolve, reject) => {
		const ApkReader = require("adbkit-apkreader");
		ApkReader.open(apkPath)
		.then((x: any) => x.readManifest())
		.then(resolve)
		.catch(reject);
	});
}

const Adb = {
	createClient
};

interface IManifestEntry {
	name: string
}

interface IManifestActivity extends IManifestEntry{
	label: string,
	intentFilters: {
		actions: IManifestEntry[],
		categories: IManifestEntry[]
	}[]
}

interface IManifest {
	versionName: string;
	package: string;
	application: {
		label: string,
		activities: IManifestActivity[],
		launcherActivities: IManifestActivity[]
	}
}

export default Adb;