import { AdbDevice } from "./AdbDevice";

export const DEVICE_MODEL_ID = {
	Oculus: "OCULUS",
	Pico: "PICO NEO",
	Unknown: "UNKNOWN"
} as const;

export type DeviceModelId = typeof DEVICE_MODEL_ID[keyof typeof DEVICE_MODEL_ID];

export class DeviceModel {
	device = null as AdbDevice;
	deviceModel = null as DeviceModel;

	constructor(public adbDevice: AdbDevice) {
		this.device = adbDevice;
	}
	
	async updateOS() {
	  	throw new Error("Upgrading the device OS is not yet possible for this device type.");
	}

	async downloadOS() {
		throw new Error("Downloading the latest OS version is not yet possible for this device type.");
	}

	async osKiosk(enableKiosk: boolean) {
		throw new Error("Enabling/Disabling kiosk mode is not yet possible for this device type.");
	}

	async getLatestOSVersion(): Promise<string> {
		throw new Error("Getting the latest OS version string is not yet possible for this device type.");
	}

	async nwEnvSettings() {
		console.log(this.deviceModel);
		throw new Error("Setting Next World environment settings is not yet possible on this device type.");
	}

	async setProps(doLog: boolean = true) {
		throw new Error("Setting props is not yet possible on this device type.");
	}

	async setDefaultPackage(packageName: string, withGuardian: boolean) {
		throw new Error("Setting Agent as default package is not yet possible on this device type.");
	}
  }