enum StdOutColor {
    Default = 0,
    Red = 31,
    Green = 32,
    Yellow = 33
}

class Logger {
    isVerbose = false;
    hasOutput = false;
    info(...args: string[]) {
        this.isVerbose && consoleLog(args);
    }
    help(...args: string[]) {
        stdout(StdOutColor.Default, args);
    }
    warn(...args: string[]) {
        stdout(StdOutColor.Yellow, args);
    }
    error(first: string|Error, ...args: string[]) {
        const errorStr = "ERROR: " + (first instanceof Error ? this.isVerbose ? first.stack : first.message : String(first) || "unknown") + args.join(" ");
        stdout(StdOutColor.Red, [errorStr]);
    }
    output(s: string) {
        this.hasOutput = true;
        stdout(StdOutColor.Default, [s]);
    }
    success(...args: string[]) {
        stdout(StdOutColor.Green, args);
    }
}

export const log = new Logger();

export function setLogLevel(on: boolean) {
    log.isVerbose = on;
}

function consoleLog(args: string[]) {
    console.log(...args);
}

function stdout(color: StdOutColor, args: string[]) {
    let str = "";
    if (color) {
        str += "\x1b[" + color + "m";
    }
    for (const arg of args) {
        str += arg + " ";
    }
    str += "\x1b[0m\n";
    process.stdout.write(str);
}