import { exit } from "process";
import AdbClient from "./AdbClient";
import * as CommandList from "./commands";
import { log, setLogLevel } from "./log";

const TITLE = "NWCommander CLI";

const cliOptions = { ...(CommandList.DEFAULT_OPTIONS as any) } as Record<string, string>;

const optionMap: [CommandList.CmdOptionsKey, string, string, object][] = [
	["device", "d", "set the device serial or index (default 0)", String],
	["verbose", "v", "sets the log output to verbose", Boolean]
];

function getCommand(name: string) {
	const commands = CommandList.getCommands();
	const cmd = commands.find(x => x.name.trim().toLowerCase() == name.trim().toLowerCase());
	return cmd;
}

function printHelpAll() {
	log.help("How to use:");
	const commands = CommandList.getCommands();
	for (const cmd of commands) {
		log.help("  " + cmd.name.padEnd(17) + cmd.description);
		if (cliOptions.verbose) {
			printArgs(cmd.arguments, "    ");
			log.help("");
		}
	}
	log.help("Options:")
	for (const option of optionMap) {
		log.help("  --" + (option[0] + " | -" + option[1]).padEnd(15) + option[2]);
	}
	process.exit(1);
}

function printArgs(argDescriptions: string[][], indent = "") {
	if (argDescriptions) {
		for (const x of argDescriptions) {
			log.help(indent + "  " + ("[" + x[0] + "]").padEnd(20) + x[1]);
		}
	}
}

function printHelpOne(methodName: string) {
	const cmd = getCommand(methodName);
	if (!cmd) {
		log.error("No command '" + methodName + "'");
		printHelpAll();
		return;
	}
	log.help(cmd.name + ": " + cmd.description);
	const argStr = cmd.arguments ? cmd.arguments.map(x => "[" + x[0] + "]").join("") : "";
	log.help("  i.e.: nwdb " + methodName + " " + argStr);
	if (cmd.arguments) {
		log.help("arguments:");
		printArgs(cmd.arguments);
	}
}

function printHelpMethods() {
	const commands = CommandList.getCommands();
	log.help("  methods are: " + commands.map(x => x.name).join("|") + "\n('nwdb help' for more info)");
}

async function run() {
	process.on("uncaughtException", e => {
		log.error(e);
		process.exit(1);
	});
	const args = process.argv.slice(2);
	for (let i=0; i<args.length; i++) {
		let key = args[i].trim();
		if (key.charAt(0) !== '-')
			continue;

		if (!key) continue;
		key = key.toLowerCase().trim();

		while (key.charAt(0) === '-') {
			key = key.substring(1);
		}

		const value = (args[i + 1] || "").trim();
		let spliceCount = 2;

		for (const [name, shortName, description, objType] of optionMap) {
			if (name === key || shortName === key) {
				cliOptions[name] = value;
				if (objType === Boolean) {
					spliceCount = 1;
					cliOptions[name] = "1";
				}
			}
		}

		args.splice(i, spliceCount);
	}
	const methodName = args.shift();
	if (!methodName || methodName === "help") {
		log.info(TITLE);
		if (args.length > 0) {
			printHelpOne(args[0]);
		} else {
			printHelpAll();
		}
		return;
	}
	const cmd = getCommand(methodName);
	setLogLevel(Boolean(cliOptions.verbose));
	if (cliOptions.verbose) {
		log.info("verbose logging...");		
	}
	if (cmd) {
		try {
			cmd.verifyArgs(...args);
		}
		catch (e) {
			log.error(e.message);
			printHelpOne(methodName);
			process.exit(1);
		}
		try {
			cmd.setOptions(cliOptions);
			const result = await cmd.run(...args);
			if (result instanceof Promise) {
				log.error(new Error("Command needs awaiting."));
			}
			if (result instanceof Array) {
				log.output(result.map(x => String(x)).join("\n"));
			}
			else if (result !== undefined) {
				log.output(String(result));
			}
			if (result === false) {
				exit(1);
			}
		} catch (e) {
			const errorMsg = typeof e === "string" ? e : e instanceof Error ? cliOptions.verbose ? e.stack : e.message : "Unknown error";
			log.error(errorMsg);
			if (cliOptions.verbose) {
				printHelpOne(methodName);
			}
			process.exit(1);
		}
		if (log.hasOutput === false) {
			log.output("OK");
		}
		process.exit(0);
	} else {
		log.help(TITLE);
		if (methodName) {
			log.error("ERROR: Unknown method '" + methodName + "'");
			printHelpMethods();
		} else {
			log.error("ERROR: No method specified");
			printHelpMethods();
		}
		process.exit(1);
	}
}

if (module === require.main) {
	run();
}

module.exports = { CommandList, run };
