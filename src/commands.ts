import AdbClient from "./AdbClient";
import { readManifest } from "./Adbkit";
import * as FS from "fs";
import { EventEmitter } from "events";

EventEmitter.prototype.setMaxListeners(12);

export const DEFAULT_OPTIONS = {
	device: "0",
	verbose: false
}
export type CmdOptions = typeof DEFAULT_OPTIONS;
export type CmdOptionsKey = keyof CmdOptions;

const commands = [] as Cmd<any>[]

export abstract class Cmd<T = string> {
	name: string;
	description: string;
	arguments?: string[][];
	options: CmdOptions = DEFAULT_OPTIONS;

	setOptions(options: Record<string, string>) {
		for (const key of Object.keys(options)) {
			(this.options as any)[key] = options[key];
		}
	}

	verifyArgs(...args: string[]) {
		if (this.arguments && this.arguments.length > 0) {
			for (let i=0; i<this.arguments.length; i++) {
				if (!args[i] && this.arguments[i][2] === undefined) {
					throw new Error("missing argument: please specify " + this.arguments[i][0]);
				}
			}
		}
	}

	async run(...args: string[]): Promise<T> {
		throw new Error("Not implemented.");
	}
}

export class DeviceCmd extends Cmd<string[]> {
	name = "devices";
	description = "get a list of attached devices";
	async run(): Promise<any> {
		const client = new AdbClient();
		const devices = await client.getDevices();
		return devices.map((x) => x.id);
	}
}

/*
// temporarily disabled due to undesirable traits
export class CloudInstallCmd extends Cmd<void> {
	name = "cloud-install";
	description = "installs an APK registered on S3";
	arguments = [["package name", "the package identifier to install"]];
	async run(...args: string[]) {
		const [packageName] = args;
		if (!packageName) throw new Error("packageName must be specified");
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		await device.installFromCloud(packageName);
	}
}
*/

export class IsInstalledCmd extends Cmd<boolean> {
	name = "installed";
	description = "checks if a package is installed";
	arguments = [["package name", "the package identifier to install"]];
	async run(...args: string[]) {
		const [packageName] = args;
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		const result = await device.isInstalled(packageName);
		return result;
	}
}

export class ListAppsAllCmd extends Cmd<string[]> {
	name = "lsapps";
	description = "get a list of all installed apps";
	arguments = [["filter", "a filter to search by", ""]]
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		const packageNames = await device.getPackageNames(args[0]);
		return packageNames;
	}
}

export class ListCatalogCmd extends Cmd<string[]> {
	name = "lscat";
	description = "get a list of catalogued apps";
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		const catalog = await device.readCatalog();
		if (this.options.verbose) {
			return catalog.experiences.map(x => JSON.stringify(x));
		}
		return Object.values(catalog.experiences).map(x => x.appId);
	}
}

export class VerifyCmd extends Cmd<string> {
	name = "verify";
	description = "returns an error if a package is NOT installed";
	arguments = [["package name", "the package identifier to verify"]];
	async run(...args: string[]) {
		const [packageName] = args;
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		const result = await device.isInstalled(packageName);
		if (!result) {
			throw new Error("Package " + packageName + " is not installed.");
		}
		return "verified " + packageName;
	}
}

export class VersionCmd extends Cmd<string> {
	name = "version";
	description = "gets the version of adb";
	async run(...args: string[]) {
		const client = new AdbClient();
		const version = await client.getVersion();
		return version;
	}
}

export class ListPacksDownloadedCmd extends Cmd<string[]> {
	name = "lspd"
	description = "lists the nextworld packs downloaded to the device";
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		return await device.getPacksDownloaded();
	}
}

export class ListPacksInstalledCmd extends Cmd<string[]> {
	name = "lspi"
	description = "lists the nextworld packs installed on the device";
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		return await device.getPacksInstalled();
	}
}

export class UninstallCmd extends Cmd<void> {
	name = "uninstall";
	description = "uninstalls an apk";
	arguments = [["package name", "the package identifier to uninstall"]];
	async run(...args: string[]) {
		const [packageName] = args;
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		await device.uninstall(packageName);
	}
}

export class InstallCmd extends Cmd<string> {
	name = "install";
	description = "installs an apk";
	arguments = [["apk path", "the local path to the apk to be installed"]];
	async run(...args: string[]) {
		const [filePath, packageName] = args;
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		const result = await device.installApk(filePath, packageName);
		if (!result) throw new Error("No result.");
		return "Installed " + result.packageName;
	}
}

/*
// temporarily disabled due to undesirable traits
export class UpdateCmd extends Cmd<void> {
	name = "update";
	description = "updates the agent";
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		await device.update();
	}
}
*/

export class ReadManifestCmd extends Cmd<string> {
	name = "manifest";
	description = "reads a manifest";
	arguments = [["apk path", "the local path to the apk to read to inspect"]];
	async run(...args: string[]) {
		const [apkPath] = args;
		const manifest = await readManifest(apkPath);
		return JSON.stringify(manifest, null, "  ");
	}
}

export class PrintCmd extends Cmd<string> {
	name = "print";
	description = "reads a text file";
	arguments = [["device path", "the device path for the text file to print"]];
	async run(...args: string[]) {
		const [filePath] = args;
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		const text = await device.readFile(filePath);
		return text;
	}
}

export class GetIdCmd extends Cmd<string> {
	name = "nwid";
	description = "gets the nextworld id";
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		const text = await device.getNextworldId();
		return text;
	}
}

export class UpgradeDeviceCmd extends Cmd<string> {
	name = "os-upgrade";
	description = "upgrades the attached device to the latest version stored on nextworld infrastructure";
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		await device.model.updateOS();
		return "Your device is now upgrading and will restart automatically.";
	}
}

export class SetNWEnvSettingsCmd extends Cmd<string> {
	name = "os-nwenv";
	description = "sets the nextworld environment settings on the device";
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		await device.model.nwEnvSettings();
		return "The Next World Environment settings have been set up successfully.";
	}
}

export class SetKioskModeCmd extends Cmd<string> {
	name = "os-kiosk";
	description = "enable or disable kiosk mode for nextworld agent on the device";
	arguments = [["true/false", "true for enable, false for disable"]];
	async run(...args: string[]) {
		let enableKioskMode: boolean;
		let sanitisedArgs = args[0].trim().toLowerCase();
		if(sanitisedArgs == "true" || sanitisedArgs == "false") {
			enableKioskMode = JSON.parse(sanitisedArgs);
		} else {
			throw new Error("Input was not true or false.");
		}

		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		await device.model.osKiosk(enableKioskMode);
		return "Kiosk mode has been turned " + (enableKioskMode == true ? "on" : "off") + ".";
	}
}

export class StartAppCmd extends Cmd<string> {
	name = "start";
	description = "starts a catalogued app";
	arguments = [["package name", "the package identifier for the app to start"]];
	async run (...args: string[]) {
		const [packageName] = args;
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		await device.startApp(packageName);
		return "started " + packageName;
	}
}

export class ShellCmd extends Cmd<string> {
	name = "shell";
	description = "forwards an adb shell command";
	arguments = [["...args", "the arguments to forward to shell"]];
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		return await device.shell(args.join(" "));
	}
}

export class AdbCmd extends Cmd<string> {
	name = "adb";
	description = "forwards an adb command";
	arguments = [["...args", "the arguments to forward to adb"]];
	async run(...args: string[]) {
		const client = new AdbClient();
		const device = await client.loadDevice(this.options.device);
		return await device.adbRun(...args);
	}
}

export class DocsCmd extends Cmd<string> {
	name = "docs";
	description = "generates the README.md for this CLI";
	async run (...args: string[]) {
		const commands = getCommands();
		const packageJson = JSON.parse(FS.readFileSync(__dirname + "/../package.json") as any) as Record<string, string>;
		let str = "# " + packageJson.description + "\n\n## Commands:\n\n";
		for (const cmd of commands) {
			str += "### " + cmd.name + "\n" + cmd.description + "\n\n";
			str += "> `nwdb " + cmd.name;
			if (cmd.arguments) {
				str += " " + cmd.arguments?.map(x => "[" + x[0] + "]").join(" ");
			}
			str += "`\n";
			if (cmd.arguments) {
				for (const arg of cmd.arguments) {
					str += "  - **[" + arg[0] + "]**: " + arg[1] + "\n";
				}
			}
			str += "\n\n";
		}
		FS.writeFileSync(__dirname + "/../README.md", str);
		return "docs generated";
	}
}

export function getCommands() {
	if (commands.length < 1) {
		for (const key in module.exports) {
			if (module.exports.hasOwnProperty(key) === false)
				continue;
			const CmdClass = (module.exports as any)[key];
			if (CmdClass && CmdClass.constructor && CmdClass.constructor.name && CmdClass.constructor === Function && CmdClass !== Cmd) {
				const cmd = new CmdClass() as Cmd<any>;
				if (cmd && cmd instanceof Cmd) {
					commands.push(cmd);
				}
			}
		}
	}
	return commands;
}
