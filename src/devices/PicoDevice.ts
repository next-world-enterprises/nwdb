import * as Paths from "../paths";
import * as Path from "path";
import { log } from "../log";
import { AdbDevice } from "../AdbDevice";
import * as FS from "fs-extra";
import Utils from '../utils';
import { DeviceModel } from "../DeviceModel";

export class PicoDeviceModel extends DeviceModel {

	constructor(public device: AdbDevice) {
		super(device);
	}

	async updateOS() {
		await this.downloadOS();

		await this.device.shell("am start -c android.intent.category.DEFAULT com.picovr.updatesystem/com.picovr.updatesystem.MainActivity --display 1");
		await this.device.wait(2000);

		await this.device.pressButton("offline_update_btn", true);
		await this.device.pressButton("btn_confirm", true);
	}

	async downloadOS() {
		let updateFile = "";
		let versionName = await this.getLatestOSVersion();
		if(versionName.endsWith(".zip") == false) versionName += ".zip";
		const existingOSFile = FS.readdirSync(Paths.LOCAL_DATA).find(name => name.includes("neo3").valueOf());
		if (existingOSFile != undefined) {
			if(existingOSFile != versionName) {
				FS.removeSync(Path.join(Paths.LOCAL_DATA, existingOSFile));
			}
		}

		updateFile = await Utils.httpsDownload(new URL(versionName, "https://app.nextworldenterprises.com/pui/"), "OS update", 1);
		log.output("\nCopying the latest OS version to the device.");
		await this.device.shell(("if [ ! -e /sdcard/dload ]; then mkdir /sdcard/dload; fi"));
		await this.device.pushFile(updateFile, "/sdcard/dload/", versionName, true, "OS update");
		if(versionName.startsWith("update_PicoNeo3_") == false) {
			await this.device.shell("mv sdcard/dload/" + versionName + " sdcard/dload/update_PicoNeo3_" + versionName);
		}
		log.info(`Successfully placed ${updateFile.substring(updateFile.lastIndexOf("\\") + 1)} on the device.`);
	}
	
	async getLatestOSVersion(): Promise<string> {
		try {
			await FS.access(Paths.LOCAL_DATA);
		} catch {
			await FS.mkdir(Paths.LOCAL_DATA);
		}

		const osVersionFile = Path.join(Paths.LOCAL_DATA, "puiversion.txt");

		if (FS.existsSync(osVersionFile) === false) {
			await Utils.httpsDownload(new URL("https://app.nextworldenterprises.com/puiversion.txt"));
		}
		const osVersionDataStr: string = FS.readFileSync(osVersionFile, {
			encoding: "utf-8",
		});
		FS.remove(osVersionFile);
		return osVersionDataStr;
	}

	async nwEnvSettings() {
		log.info("Setting up the Next World Pico Environment settings.");
		log.info("----------------------------------------------------");
		// Set props for the default Next World Pico environment.
		await this.device.setVolume(75);
		await this.setProps();
		await this.setSleepDelayNever();
		await this.enableMiracasting();
		await this.enableGlobalAntiDispersion();
		log.info("----------------------------------------------------");
	}

	async osKiosk(enableKiosk: boolean) {
		if(enableKiosk == true) {
			const appDataStr: string = FS.readFileSync(Paths.LOCAL_APPS, { encoding: "utf-8" });
			await this.setDefaultPackage(JSON.parse(appDataStr).agent.package, false);				
		} else {
			await this.device.shell("if [ -f /sdcard/config.txt ]; then rm -f /sdcard/config.txt; fi");
		}	
	}

	async setProps(doLog: boolean = true) {
		var props = new Map<string, string>([
			["persist.pvr.mulkey.enable 0", 		""],
			["persist.pvr.2dtovr.button_all 0", 	""],
			["persist.pvr.2dtovr.button_back 1",	""],
			["persist.pvr.sleep_by_static 0",		"Auto Sleep has been set to 'off'."],
			["persist.pvr.mono.castmode 1",			"Screencasting & Screen Recording Aspect Ratio has been set to '16:9'."],
			["persist.pvr.outproxy sink",			"Audio Settings for Screencasting has been set to 'output from headset'."],
			["persist.pvr.raycast.enable 1",		"UI Clarity Optimisation has been set to 'on'."],
			["persist.accept.systemupdates 0",		"System PUI Updates have been 'disabled'."]
		]);

		props.forEach(async (description, prop) => {
			await this.device.shell("setprop " + prop);
			if (description != "" && doLog == true) log.info(description);
		});
	}

	async setSleepDelayNever() {
		// persist.psensor.sleep.delay prop does not exist anymore so we need to manually set it via UI.
		await this.device.shell("am start -n com.pvr.developmentsettings/.UserConfigSettingsActivity");
		// The id of the UI element is "title" so is not unique, need to use the element's name instead.
		let foundSleepTimeoutElement = await this.device.searchForElement("System Sleep Timeout", 5);
		if(foundSleepTimeoutElement) {
			// The id of the UI element is "title" so is not unique, need to use the element's name instead.
			await this.device.pressButton("Never");
			await this.device.pressButton("button1");
			await this.device.shell("input keyevent 'KEYCODE_BACK'");
			log.info("System Sleep timeout has been set to 'never'.");
		} else {
			throw new Error("Element 'System Sleep Timeout' could not be found by the maximum tries.");
		}
	}

	async enableMiracasting() {
		await this.device.shell("am start -a android.settings.CAST_SETTINGS");
		await this.device.wait();
		await this.device.pressButton("ImageButton");
		await this.device.pressButton("CheckBox");
		await this.device.wait();
		await this.device.shell("input keyevent 'KEYCODE_BACK'");
		log.info("Wireless Display has been 'enabled' for screencasting.");
	}

	async enableGlobalAntiDispersion() {
		// Miracasting is disabled by default so we want to enable it again.
		await this.device.shell("am start -a com.pvr.tobsettings.main com.pvr.tobservice/.tobsettings.TobSettingsActivity");
		await this.device.shell("input keyevent 'KEYCODE_DPAD_RIGHT'");
		await this.device.shell("input keyevent 'KEYCODE_DPAD_RIGHT'");
		// The id of the UI element is "title" so is not unique, need to use the element's name instead.
		let foundGlobalADElement = await this.device.searchForElement("Global anti-dispersion", 6, false);
		if(foundGlobalADElement) {
			await this.device.shell("input keyevent 'KEYCODE_TAB'");
			await this.device.shell("input keyevent 'KEYCODE_ENTER'");			
			await this.device.pressButton("btn_confirm");
			log.info("Miracasting has been re-enabled.");
		} else {
			throw new Error("Element 'Global anti-dispersion' could not be found by the maximum tries.");
		}
	}

	async setDefaultPackage(packageName: string, withGuardian: boolean) {
		const openGuide = withGuardian ? 1 : 0;
		const configStr = [
			"open_guide:" + openGuide,
			"------",
			"home_pkg:" + packageName.trim(),
			"------"
		].join("\n");
		const tempPath = Path.join(Paths.LOCAL_DATA, "config.txt");

		FS.writeFileSync(tempPath, configStr, { encoding: "utf-8" });
		await this.device.pushFile(tempPath, "/sdcard/", "config.txt");
		FS.unlinkSync(tempPath);
	}
}