import * as Path from "path";

export const PACKAGE_AGENT = "com.nextworld.agent";
export const PACKAGE_HOME = "com.NextWorld.NextWorldMenu";

export const DEVICE_DATA = "/sdcard/Android/data";
export const DEVICE_AGENT_FILES = DEVICE_DATA + "/" + PACKAGE_AGENT + "/files";
export const DEVICE_AGENT_CATALOG = DEVICE_AGENT_FILES + "/catalog.data";
export const DEVICE_HOME_FILES = DEVICE_DATA + "/" + PACKAGE_HOME + "/files";
export const DEVICE_HOME_PACKS = DEVICE_HOME_FILES + "/packs";

export const LOCAL_DATA = Path.resolve(__dirname, "..", "data");
export const LOCAL_APPS = Path.join(LOCAL_DATA, "apps.json");

export const URL_APPS_JSON = "https://app.nextworldenterprises.com/apps.json";

export function parse(...pathParts: string[]) {
    if (pathParts.length > 0) {
        let s = Path.join(...pathParts).trim();
        s = s.replace(/\\/g, "//");
        s = s.replace(/\/+/g, "/");
        return s;
    }
    return null;
}

export function parseDevicePath(...pathParts: string[]) {
    let s = parse(...pathParts);
    if (s.startsWith("/")) {
        s = s.substring(1);
    }
    if (s.startsWith("com.")) {
        s = DEVICE_DATA + "/" + s;
    }
    return s;
}

export function basename(path: string) {
    return Path.basename(path);
}

export function dirname(path: string) {
    return Path.dirname(path);
}