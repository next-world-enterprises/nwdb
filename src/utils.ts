import * as OS from "os";
import * as Path from "path";
import { Stream } from "stream";
import * as FS from "fs-extra";
import * as Https from "https";
import * as Paths from "./paths";
import { log } from "./log";

export default class Utils {
	static httpsDownload(url: URL, purpose: string = "", percentUpdateFrequency: number = 10, childFolder: string = "") {
		return new Promise<string>((resolve, reject) => {	
			const fileName = url.toString().substring(url.toString().lastIndexOf("/") + 1);
			// Check that the file has not previously been downloaded before hitting S3.
			FS.access(Path.join(Paths.LOCAL_DATA, childFolder, fileName), FS.constants.F_OK, (err) => {
				if (err === null) {
					resolve(Path.join(Paths.LOCAL_DATA, childFolder, fileName));
				} else {
					var totalLength = 0;
					var downloadedLength = 0;
					var percentComplete = 0;
					var lastPercentageShown = -1;

					const req = Https.request(url, (res) => {
						totalLength = parseInt(res.headers["content-length"]);
					});
					req.end();	

					Https.get(url, (res: { pipe: (arg0: FS.WriteStream) => any; }) => {
						const filePath = FS.createWriteStream(Path.join(Paths.LOCAL_DATA, childFolder, fileName));
						res.pipe(filePath);
											
						filePath.on("drain", () => {
							downloadedLength = filePath.bytesWritten;
							percentComplete = Math.round((downloadedLength / totalLength) * 100);

							if (percentComplete % percentUpdateFrequency == 0 && percentComplete > lastPercentageShown) {
								if (purpose != "") log.output(`${purpose} is ${percentComplete}% downloaded.`);
								lastPercentageShown = percentComplete;
							}
						});
						filePath.on("finish", () => {
							filePath.close();
							resolve(filePath.path as string);
						});
						filePath.on("error", err => {
							FS.unlink(filePath.path);
							reject("error: " + err.message);
						});
					});
				}
			});
		});
	}
}

export function absPath(dataFile: string) {
	if (dataFile.startsWith("/") === false) {
		dataFile = Path.join(Path.dirname(__dirname), "data", dataFile);
	}
	return dataFile;
}

export function getBinPath() {
	let binPath: string;
	const platform = OS.platform();
	if (platform === "win32") {
		binPath = Path.join(__dirname, "..", "bin", "win", "adb.exe");
	} else if (platform === "darwin") {
		binPath = Path.join(__dirname, "..", "bin", "mac", "adb");
	}
	return binPath;
}

export function readDeviceStream(stream: Stream) {
	return new Promise<string>((resolve, reject) => {
		let result = "";
		stream.on("data", chunk => result += chunk);
		stream.on("end", (e: any) => {
			resolve(result.trim());
		});
		stream.on("error", e => {
			reject(e);
		});
		stream.on("close", e => {
			resolve(result);
		});
	});
}